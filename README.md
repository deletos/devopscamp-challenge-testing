# devopscamp - adidas OPS challenge testing

### Challenge
Your mission, should you choose to accept it, is to create a testing setup.

### Specifications
- Setup shall test simple wordpress page http://devopsws.crm.aws.ds.3stripes.net/
	- functional testing
	- performance testing
- Test shall create a meaningful report
- Useraccount: devopscamp / trustinautomation

#### Optional
- Configs to be stored in this gitlab project
- collecting and visualising test/performance data
- distributed testing (several hosts)


